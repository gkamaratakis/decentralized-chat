const crypto = require('crypto') //βιβλιοθήκη για να κάνουμε hash το ID μας και τα IDs των μηνυμάτων
const Swarm = require('discovery-swarm') //χάρη αυτής της βιβλιοθήκης τα peers μας βλέπουν το ένα το άλλο και στην ουσία συμμετέχουν σε ένα κανάλι για να κάνουν multicast τα μηνύματα.
const defaults = require('dat-swarm-defaults') //
const getPort = require('get-port') //το δίκτυο στο οποίο τρέχει είναι το localhost και γι'αυτό τα peers μας είναι random unused ports. Το εκάστοτε process παίζει σ'αυτό το port
const readline = require('readline') //βιβλιοθήκη του readline function

/**
 * Here we will save our TCP peer connections
 * using the peer id as key: { peer_id: TCP_Connection }
 */
var peers = []; //{}
var connSeq = 0 // Counter for connections, used for identify connections
var sortArr = [];
var consensus = false;

const myId = crypto.randomBytes(32) // Peer Identity, a random hash for identify your peer
console.log('=======================================================================================');
console.log('Your identity: ' + myId.toString('hex')); //32 bytes = 64 χαρακτήρες σε hex 
console.log('=======================================================================================');

var sequencer_id; //ποιός είναι ο sequencer
var latest_sequence = 0; //αυτό ανήκει στο process , για να δείς το sequence το δικο του ποτε σταμάτησε ...
var counter = 0; //αυτό είναι του sequencer
var myCounter = 0; //αυτό ανήκει στο process 

var lsn_sent = 0;
var mid;
var in_message_id;
var message_id;
var the_message;
var sqn, midq; //sequence_number kai message_id_in_queue
var voith = false; //voithitiki boolean metavliti
var element; //stin ousia to record tou myQueue pou exei ta stoixeia enos minimatos
var myQueue = [];


var message2;

function delayX(x) {

    setTimeout(function() {}, x);

}
/*====================================================================
                             ELECTIONS
====================================================================*/

function arrlen() { //αυτή η συνάρτηση τρέχει και μας δίνει πόσοι χρήστες είναι Online.
    sortArr = [];
    sortArr.push(myId.toString('hex')); //στον πίνακα βάζουμε και τον εαυτό μας για να γίνει το SORT γιατί ο πίνακας των peers μας δίνει μόνο των άλλων τα IDs
    for (var id in peers) {
        sortArr.push(id); //εδω βάζουμε όλα peers_ids σε ενα πίνακα για να τα κανουμε SORT και να βγάλουμε LEADER
    }
    total = sortArr.length
    return total
}

function sort_peers() {
    sortArr = [];
    sortArr.push(myId.toString('hex')); //στον πίνακα βάζουμε και τον εαυτό μας για να γίνει το SORT γιατί ο πίνακας των peers μας δίνει μόνο των άλλων τα IDs
    for (var id in peers) {
        sortArr.push(id); //εδω βάζουμε όλα peers_ids σε ενα πίνακα για να τα κανουμε SORT και να βγάλουμε LEADER
    }
    sortArr.sort();
}

function sequencer_election() {
    sort_peers();
    for (var id in peers) {
        peers[id].conn.write("pll-" + sortArr[0]) //εδω γίνετε η αποστολή του ψηφου ΜΟΥ σε όλους
        delayX(200);
    }
    sequencer_id = sortArr[0]; //ο κορυφαίος μετά το SORT είναι ο LEADER----------gia na kanoyme debug ----
    /*  ΤΑ ΑΠΟΤΕΛΣΜΑΤΑ ΘΑ ΑΝΑΚΥΡΗΞΟΥΝ LEADER
        ΟΠΟΤΕ ΣΤΟ onDATA ΘΑ ΓΙΝΕΙ Η ΔΟΥΛΕΙΑ  */
}

/*====================================================================
                          TELOS   ELECTIONS
====================================================================*/



/*====================================================================
                                Sequencer
====================================================================*/

function findObjectByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

function compareArrayOfObject(a, b) {
    const mA = a.sqn;
    const mB = b.sqn;
    let comparison = 0;
    if (mA > mB) {
        comparison = 1;
    } else if (mA < mB) {
        comparison = -1;
    }
    return comparison;
    //return comparison * -1; //reverse the results
}


function broadcast_sequence(counter, message_id) {
    //if (sequencer_id == myId.toString('hex')) { //mono an eisai o sequencer kaneis broadcast , axreiasto giati ginete elegxos stin main
    counter++ //afksanoume to latest kai meta broadcast
    //edw to broadcast
    for (var id in peers) {
        peers[id].conn.write("bfs-" + message_id + "-" + counter)
        delayX(200);
    }
    //if (myQueue.length != 0) { //den tha eprepe afto ...alla kapou exei bug kai skaei
    element = findObjectByKey(myQueue, 'id', message_id); //edw enimerwnei ton pinaka tou sxetika o sequencer
    // if (element != "NULL") {
    element.sqn = counter; //edw enimerwnei ton pinaka tou sxetika o sequencer
    //  }

    latest_sequence = counter;
    return counter
        // }
}
//}


function putMessageInQueue(messageID, the_message, sender_id, sequence_number) {
    //if sequence number == 0 tote simainei oti den exei dwsei o sequencer seira akoma...menei ekei 
    myQueue.push({ id: messageID, message: the_message, sid: sender_id, sqn: sequence_number });
}

function printMessagesInQueue() {
    console.log(myQueue.sort())
}

function show() {
    ////myCounter=latest_sequence
    //log("prospathw na kanw show")
    var newQueue = myQueue.sort(compareArrayOfObject);
    if (newQueue.length != 0) {
        //log("den einai adeios")
        if (newQueue[0].sqn) { //an o prwtos gia deliver einai o counter+1 tote proxwra kai an o pinakas...
            //log("komple?")
            var temp = newQueue[0].sid;
            log(temp.toString().substring(0, 4) + "->" + newQueue[0].message + ":" + newQueue[0].sqn); //kane deliver...
            newQueue.splice(0); //remove message from queue
            // myCounter++ //afksanoume ton metriti tou ti exei seira gia deliver
        } else {
            // console.log("\033[31m", newQueue.sort());
        }
    }
    /*
        newQueue.forEach(function(arrayItem) {
            if (arrayItem.sqn <= latest_sequence)
                log(arrayItem.sid + "->" + arrayItem.message + ":" + arrayItem.sqn);
        });
    */
}

/*====================================================================
                            Telos Sequencer
====================================================================*/

// reference to redline interface
var rl
    /*====================================================================
    Function for safely call console.log with readline interface active
    ====================================================================*/
function log() {
    if (rl) {
        rl.clearLine()
        rl.close()
        rl = undefined
    }
    for (var i = 0, len = arguments.length; i < len; i++) {
        console.log(arguments[i])
    }
    askUser()
}

/*=================================================================
Function to get text input from user and send it to other peers
=================================================================*/
const askUser = async() => {
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    rl.question('>>> ', message => { //to prompt tou sender
        readline.moveCursor(process.stdout, -100, -1) //svinei ews 1 grammi keimeno apo afto pou eggrapses etsi wste na exoume to idio view
            //readline.cursorTo(process.stdout, -5);
        if (message == "wis-") { /// Who Is Sequencer   -     cheatcode
            log("+++++++++S_ID++++++++++++" + sequencer_id + "+++++++++S_ID++++++++++++"); //to exoume kai edw kai sto onData gia na emfanizetai kai ston xristi ton idio
        } else if (message == "-arr") { /// What Is the array of the remainants   -     cheatcode
            sort_peers(); //prin mou dwseis apotelesma ananewse ton pinaka kai vgale leader
            log("---------sortArr------------" + sortArr.toString() + "---------sortArr------------")
        } else if (message == "-mid") { /// What Is my id   -     cheatcode
            log("---------myID------------" + myId.toString('hex') + "---------myID------------")
        } else if (message == "cnt-") { // What Is The counter   -     cheatcode
            log("+++++++++TIME++++++++++++" + latest_sequence + "+++++++++TIME++++++++++++"); //to exoume kai edw kai sto onData gia na emfanizetai kai ston xristi ton idio
        } else if (message == "-len") { // How many are in chat   -     cheatcode
            log("+++++++++posoi++++++++++++" + arrlen() + "+++++++++posoi++++++++++++"); //vlepoume posoi einai mesa
        } else if (message == "-ell") { /// Make elections   -     cheatcode
            sequencer_election();
        } else if (message == "msq-") { /// Print myQueue   -     cheatcode
            printMessagesInQueue();
        } else {

            mid = crypto.randomBytes(32).toString("hex")
            putMessageInQueue(mid, message, myId.toString("hex"), 0); //if eisai o apostoleas valto sto queue sou
            //printMessagesInQueue();
            message2 = "trm-" + mid + "-" + message //  an einai minima apo ton xristi
            for (var id in peers) { // Broadcast to peers
                //PROSOXI: edw ginete to ousiodes broadcast....otan grapseis panw sto connection tou peer afto tha emfanistei kai se ola ta nodes
                peers[id].conn.write(message2) //εδω γίνετε το broadcast   
                delayX(200);
                //to dinoume se ola ta nodes   enw tha prepei na to vazoume sto buffer tou kathenos QUEUE kai meta na ginete read apo to TOB
            }
            /* i parakatw metavliti VOITH einai gia na mporesw na ta valw se seira ta gegonota...prwta na fevgei to minima se olous kai meta na ekteleite sqn_broadcast gia afto */
            if (sequencer_id == myId.toString('hex')) {
                counter = broadcast_sequence(latest_sequence, mid);
                //if (sequencer_id == myId.toString('hex')) {
                //printMessagesInQueue();//to thelw
                show();
            } else {
                //printMessagesInQueue();//to thelw
                show();
            }
            //!!!!!!!!!!!!!!!!!edw itan to broadcast gia tous peers


        }




        rl.close()
        rl = undefined
        askUser()
    });
}

/*===================================================================== 
 * Default DNS and DHT servers
 * This servers are used for peer discovery and establishing connection
======================================================================*/
const config = defaults({
    // peer-id
    id: myId,
})

/*===================================================================
discovery-swarm library establishes a TCP p2p connection and uses
discovery-channel library for peer discovery
====================================================================*/

const sw = Swarm(config);

//start of async block
(async() => {
    // Choose a random unused port for listening TCP peer connections
    const port = await getPort();
    /*με την await όλο το script τρέχει κανονικά μόνο αυτό το block (async block) περιμένει μέχρι να έχει απάντηση...
    έτσι δεν κολλάει το script και έτσι είναι ασύγχρόνο
    */
    sw.listen(port);
    console.log('Listening to port: ' + port)
        /**
         * The channel we are connecting to.
         * Peers should discover other peers in this channel
         */
    sw.join('our-fun-channel') //το κανάλι στο οποίο ακούνε όλα τα peers.

    //sequencer_election(); //an den to valeis edw o prwtos pou mpainei einai undefined giafto des parakatw....
    sequencer_id = myId.toString('hex'); //edw einai i stigmi pou mpainei kai den kserei oti exei allous...opote vazei ton eafto tou LEADER
    //to provlima einai oti tin dedomeni stigmi den kserei tous allous peers opote dn ginete na kanei leader election

    sw.on('connection', (conn, info) => {

            // Connection id
            var seq = connSeq;
            const peerId = info.id.toString('hex');
            //log(`Connected #${seq} to peer: ${peerId}`);
            //με το που μπεις στο chat σαν peer κάνε elections ***** ή ζήτα τοn leader ???????
            //to na zitisw leader den mou vgainei....kanw elections pali.....
            sequencer_election(); //edw doulevei - apla idanika theloume na rwtame poios einai o sequencer kai oxi na eklegoume ksana
            // Keep alive TCP connection with peer
            if (info.initiator) {
                try {
                    conn.setKeepAlive(true, 600);
                } catch (exception) {
                    log('exception', exception);
                }
            }

            conn.on('data', data => {

                    // log(data.toString())
                    // Here we handle incomming messages
                    // event του receiver
                    var x = data.toString().split("-");
                    /*
                    if (data.toString().includes("bfs-")) {
                        var xx = data.toString().indexOf("bfs-")
                        x = data.toString().substring(xx, xx + 4);
                    }
*/
                    //console.log(data.toString()); ///////////////////uncomment this to see all packets/messages
                    var checkTotalVotersWithConsensus = 0;
                    switch (x[0]) {
                        case "pll": // poll msg
                            if (x[1] == sequencer_id) {
                                //console.log(data.toString().substring(4) + " - " + sequencer_id)
                                //EDW EINAI OI EPIKOINWNIA OTI SIMFWNOUME GIA TON LEADER -TOTE TIPOTA....EIMASTE SE KALO DROMO
                                checkTotalVotersWithConsensus++;
                            } else {
                                for (var id in peers) {
                                    peers[id].conn.write("nsq-") //εδω σημαίνει ότι κάτι δεν πηγε καλά και ζητάω απο όλους να επαναλαβουν εκλογές
                                        // delayX(200);
                                }

                            }
                            if ((checkTotalVotersWithConsensus + 1) == arrlen()) { //to +1 einai gia na simperilavw kai mena
                                consensus = true; //an psifisan oloi kai simfwnoun
                                checkTotalVotersWithConsensus = 0;
                            } else {
                                consensus = false;
                                checkTotalVotersWithConsensus = 0;
                            }
                            /*εδω έχουμε ότι αφου βρήκαμε leader , o leader τι χρειάζεται να ξέρει ???  */
                            if (consensus) {

                                for (var id in peers) {
                                    peers[id].conn.write("dos-") //zhtaw na mou dwsoun....einai sto logic bus etsi alliws o neos otan mpainei den kserei ti na kanei..
                                        // delayX(200);          ///tha mporousa apla oloi apo monoi tous an yparxei consensus na stelnoun to LSN ston sequencer
                                }
                                /**/
                                /*
                                for (var id in peers) {
                                    if (id == sequencer_id) {
                                        peers[id].conn.write("lsn-" + latest_sequence) //edw  stelnoun oloi tou sequencer ton arithmo tous
                                            //mono o sequencer tha to lavei
                                    }
                                }
                                */
                            }

                            break;
                        case "nsq": //NoSequencer - ksanakanoume ekloges - den eixame simfwnia   -     cheatcode
                            sequencer_election(); //kati pige strava ...dinoume se olous minima na epanalavoun psifoforia
                            break;

                        case "dos": //endoEpikoinwnia---dwste mou to latest_sequence sas
                            for (var id in peers) {
                                if (id == sequencer_id) {
                                    peers[id].conn.write("lsn-" + latest_sequence) //edw  stelnoun oloi tou sequencer ton arithmo tous
                                        //mono o sequencer tha to lavei
                                }
                            }
                            break;
                        case "msq": //Print myQueue   -     cheatcode
                            printMessagesInQueue()
                            break;

                        case "wis": //Who Is Sequencer   -     cheatcode
                            log("+++++++++S_ID++++++++++++" + sequencer_id + "+++++++++S_ID++++++++++++");
                            break;

                        case "cnt": //What Is The counter   -     cheatcode
                            log("+++++++++TIME++++++++++++" + latest_sequence + "+++++++++TIME++++++++++++");
                            break;

                        case "trm": //the real message
                            //edw tha erxontai gia na vgainoun se seira
                            //fevgei san real message alla prepei na filtraristoun kai meta na ektipothoun
                            in_message_id = x[1];
                            the_message = x[2]; //69 gia na apofigoume tin "~"

                            //log(peerId + ' -> ' + in_message_id + " : " + the_message)
                            putMessageInQueue(in_message_id, the_message, peerId, 0); //if eisai o receiver valto sto queue sou
                            if (sequencer_id == myId.toString('hex')) {
                                counter = broadcast_sequence(latest_sequence, in_message_id);
                                //printMessagesInQueue();//to thelw
                                show();
                            }
                            break;

                        case "lsn": //last sequence number request
                            //console.log("mpika");
                            lsn_sent = x[1]; ////apo to telos tou anagnwristikou kai meta
                            //log("mou steilane: " + lsn_sent) //gia debug
                            if (latest_sequence < lsn_sent) { // an oxi , tipota....kratame to diko mas
                                //console.log("mpika2");
                                // if (sequencer_id == myId.toString('hex')) {
                                //log("---------------------------------------" + lsn_sent);//gia debug
                                latest_sequence = lsn_sent;
                                counter = latest_sequence;
                                //  }
                            }
                            //console.log(peerId + ' -> ' + lsn_sent + " , i dikia mou timi:" + latest_sequence)
                            break;

                        case "bfs": //broadcast apo sequencer
                            // PROSOXI.....NA MPEI KAI APO TON IDIO TON SEQUENCER MINIMA APO TON EAFTO TOU
                            //kai otan ginei afto tha prepei na allaksei kai se mas kai se olous to latest_sequence number....  
                            sqn = x[2]; //69 gia na apofigoume tin "~"
                            midq = x[1];

                            // if (myQueue.length != 0) { //den prepei alla to kanoume gia ta crash
                            element = findObjectByKey(myQueue, 'id', midq);
                            // console.log("e: " + element.id + " q: " + myQueue + " sqn:" + sqn)
                            //   if (element != "NULL") {
                            element.sqn = sqn;
                            // }
                            //printMessagesInQueue();//to thelw

                            latest_sequence = sqn;
                            //edw tha prepei na ginete o elegxos PWS kai GIATI kai TI kanoume deliver
                            show(); //tha doume an to thelw
                            //}

                            break;
                    }
                }) //end of onData event
            conn.on('close', () => {
                    // Here we handle peer disconnection
                    // If the closing connection is the last connection with the peer, removes the peer
                    //connSeq-- //grg
                    //log(`Connection ${seq} closed, peer id: ${peerId}`)
                    if (peers[peerId].seq === seq) {
                        delete peers[peerId]
                        if (sequencer_id == peerId) {
                            sequencer_election(); //εαν αυτός που έφυγε απο το chat ήταν ο sequencer ξανακάνε εκλογές
                        }
                    }
                }) //end of onCloseConnection event
                // Save the connection
            if (!peers[peerId]) {
                peers[peerId] = {}
            }
            peers[peerId].conn = conn
            peers[peerId].seq = seq
            connSeq++

        })
        //end of onConnection event

    askUser(); // Read user message from command line


})()
//end of async block